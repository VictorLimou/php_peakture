CREATE DATABASE peakture;

CREATE TABLE users(user_id serial primary key, user_name varchar(256), user_mail varchar(256), user_pass varchar(256));

CREATE TABLE picts(pict_id serial primary key, pict_name varchar(256), pict_data text, pict_date date, user_id integer references users(user_id));

CREATE TABLE tags(tag_id serial primary key, tag_name varchar(256));

CREATE TABLE have_tag(pict_id integer references picts(pict_id), tag_id integer references tags(tag_id), primary key(pict_id,tag_id));