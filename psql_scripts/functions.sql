CREATE OR REPLACE FUNCTION addImage(var_name varchar(256), var_data text, var_user varchar(256)) RETURNS void AS $$
DECLARE
    var_id int;
BEGIN
    SELECT user_id INTO var_id FROM users WHERE user_name LIKE var_user;
    INSERT INTO picts(pict_name,pict_data,pict_date,user_id) values(var_name,var_data,now(),var_id);
END;
$$ LANGUAGE plpgsql;