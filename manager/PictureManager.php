<?php
require './classes/Picture.php';

class PictureManager{

    private $list = array();

    public function __construct($pdo){
        $construct = $pdo->prepare("SELECT P.pict_name,P.pict_data,P.pict_date,U.user_name from picts P join users U using(user_id)");
        $construct->execute();

        foreach ($construct->fetchAll() as $row) {
            $this->list[] = new Picture($row['pict_name'],$row['pict_data'],$row['pict_date'],$row['user_name']);
        }   
    }

    public function affichage() {
        $compt = 0;
        while(isset($this->list[$compt])){
            echo("
                <div class='col-md-3 my-2'>
                    <div class='card bg-dark text-light' style='width: 16rem; height: 16rem;'>
                        <img class='card-img-top' src='{$this->list[$compt]->getData()}' alt='Card image cap' style='min-height: 9rem;'>
                        <div class='card-body'>
                            <h5 class='card-title'>{$this->list[$compt]->getName()}</h5>
                            <h6 class='card-subtitle mb-2 text-muted'>{$this->list[$compt]->getUser()} / {$this->list[$compt]->getDate()}</h5>
                        </div>
                    </div>
                </div>
            ");
            $compt++;
        }
    }

    public function addImage($file,$name,$user,$pdo){
        if($name == ""){
            $name = strtok($file['file']['name'],"."); //Si aucun nom n'est défini par l'utilisateur
        }

        $type = $file['file']['type'];
        $data = file_get_contents($file['file']['tmp_name']);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data); //ici les donnée de l'image dont le chemin est passé en paramètre sont converties en base64

        $add = $pdo->prepare("SELECT addImage(:name,:data,:user);");
        $add->bindParam(':name',$name);
        $add->bindParam(':data',$base64);
        $add->bindParam(':user',$user);
        $add->execute();
    }
}
?>