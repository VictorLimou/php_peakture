<?php

class User{

    private $id;
    private $name;
    private $mail;
    private $pass;

    public function __construct(int $id, string $name, string $mail, string $pass){
        $this->id = $id;
        $this->name = $name;
        $this->mail = $mail;
        $this->pass = $pass;
    }

/*
   _____ ______ _______ _______ ______ _____  
  / ____|  ____|__   __|__   __|  ____|  __ \ 
 | |  __| |__     | |     | |  | |__  | |__) |
 | | |_ |  __|    | |     | |  |  __| |  _  / 
 | |__| | |____   | |     | |  | |____| | \ \ 
  \_____|______|  |_|     |_|  |______|_|  \_\
*/
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getMail()
    {
        return $this->mail;
    }

    public function getPass()
    {
        return $this->pass;
    }

/*
   _____ ______ _______ _______ ______ _____  
  / ____|  ____|__   __|__   __|  ____|  __ \ 
 | (___ | |__     | |     | |  | |__  | |__) |
  \___ \|  __|    | |     | |  |  __| |  _  / 
  ____) | |____   | |     | |  | |____| | \ \ 
 |_____/|______|  |_|     |_|  |______|_|  \_\
*/
    public function setId(int $id)
    {
        $this->id = $id;
    }
    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setMail(string $mail)
    {
        $this->mail = $mail;
    }
    public function setPass(string $pass)
    {
        $this->pass = $pass;
    }
}
?>