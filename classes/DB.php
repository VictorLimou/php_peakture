<?php

class DB{
    private $host;
    private $port;
    private $dbname;
    private $user;
    private $password;
    private $pdo;

    public function __construct(string $host, int $port, string $dbname, string $user, string $password){
        $this->host = $host;
        $this->port = $port;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
    }

    public function createPDO(){
        try {
            $db = new PDO("pgsql:host={$this->host};port={$this->port};dbname={$this->dbname};user={$this->user};password={$this->password};");
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        $this->pdo = $db;
    }

    public function getPDO(){
        return $this->pdo;
    }


    public function signinUser($mail, $password, $pseudo){
        htmlspecialchars($mail);
        htmlspecialchars($pseudo);
        htmlspecialchars($password);

        $password = password_hash($password, PASSWORD_DEFAULT);

        $r = $this -> getPDO()->prepare("INSERT INTO users(user_name, user_mail, user_pass) VALUES (:pseudo, :mail, :pass)");
        $r->bindParam(":pseudo", $pseudo);
        $r->bindParam(":mail", $mail);
        $r->bindParam(":pass", $password);
        $r -> execute();
    }

    public function verifyLogin($mail, $pass){
        $r = $this->getPDO()->prepare("SELECT user_pass FROM users WHERE user_mail LIKE :mail");
        $r -> bindParam(":mail", $mail);
        $r -> execute();
        $passBDD = $r -> fetch();
        $passBDD = $passBDD[0];
        
        return password_verify($pass, $passBDD); // Pour vérifier le pass en BDD    
    }
}
?>