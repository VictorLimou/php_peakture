<?php

class Loader{
    public static function load($loadParam){
        foreach($loadParam as $item){
            if($item == "DB"){
                require_once './classes/DB.php';
            }
            else if($item == "Classes"){
                require_once './classes/User.php';
                require_once './classes/Picture.php';
                require_once './classes/Tag.php';
            }
            else if($item == "Manager"){
                require_once './manager/UserManager.php';
                require_once './manager/PictureManager.php';
                require_once './manager/TagManager.php';
            }
        }
    }
}
?>