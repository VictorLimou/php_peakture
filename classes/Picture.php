<?php

class Picture{


    private $name;
    private $data;
    private $date;
    private $user;

    public function __construct(string $name, string $data, string $date, string $user){
        $this->name = $name;
        $this->data = $data;
        $this->date = $date;
        $this->user = $user;
    }

/*
   _____ ______ _______ _______ ______ _____  
  / ____|  ____|__   __|__   __|  ____|  __ \ 
 | |  __| |__     | |     | |  | |__  | |__) |
 | | |_ |  __|    | |     | |  |  __| |  _  / 
 | |__| | |____   | |     | |  | |____| | \ \ 
  \_____|______|  |_|     |_|  |______|_|  \_\
*/
    public function getName()
    {
        return $this->name;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getDate()
    {
        return $this->date;
    }
    public function getUser()
    {
        return $this->user;
    }

/*
   _____ ______ _______ _______ ______ _____  
  / ____|  ____|__   __|__   __|  ____|  __ \ 
 | (___ | |__     | |     | |  | |__  | |__) |
  \___ \|  __|    | |     | |  |  __| |  _  / 
  ____) | |____   | |     | |  | |____| | \ \ 
 |_____/|______|  |_|     |_|  |______|_|  \_\
*/
    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setData(string $data)
    {
        $this->data = $data;
    }
    public function setDate(string $date)
    {
        $this->date = $date;
    }
    public function setUser(string $user)
    {
        $this->userID = $user;
    }
}
?>